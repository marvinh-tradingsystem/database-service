FROM debian:buster AS buildstage
# update the package manager cache and upgrade all packages
RUN apt-get -y update
# first setup tools needed for debugging
RUN apt-get --no-install-recommends -y install less
# now setup packages needed to run the application
RUN apt-get --no-install-recommends -y install openjdk-11-jdk
# last setup packages for building the application
RUN apt-get --no-install-recommends -y install maven
# now build the application
RUN mkdir /build
WORKDIR /build
COPY src/main/java/* /build/src/main/java/
COPY src/main/resources/* /build/src/main/resources/
COPY pom.xml /build/
RUN mvn clean package
# Now make a new layer containing only the necessary stuff to run the application
FROM debian:buster
RUN mkdir /database \
&& apt-get -y update \
&& apt-get --no-install-recommends -y install openjdk-11-jdk \
&& rm -rf /var/lib/apt/lists/*
COPY --from=buildstage /build/target/database-service.jar /build/target/
VOLUME /database
EXPOSE 8081
ENTRYPOINT java -jar /build/target/database-service.jar --database.file=/database/database.h2