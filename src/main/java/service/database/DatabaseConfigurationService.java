package service.database;

import databaseconnector.api.sql.SQLDatabaseConnection;
import databaseconnector.api.sql.SQLSchema;
import databaseconnector.impl.H2PersistentDatabaseConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import service.database.schema.DatabaseSchema;

import java.io.File;

@Lazy
@Configuration
@Service
public class DatabaseConfigurationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseConfigurationService.class);

    private File databaseFile;

    @Bean(name="database.connection")
    public SQLDatabaseConnection databaseConnection(){
        return new H2PersistentDatabaseConnection(databaseFile.getAbsoluteFile());
    }

    @Bean(name="database.schema")
    public SQLSchema schema(){
        return new DatabaseSchema();
    }

    @Bean(name = "database.file")
    public File getDatabaseFile(){
        return databaseFile;
    }

    public void setDatabaseFile(File databaseFile){
        this.databaseFile = databaseFile;
        LOGGER.info(String.format("Database file set: '%s'", databaseFile));
    }
}
