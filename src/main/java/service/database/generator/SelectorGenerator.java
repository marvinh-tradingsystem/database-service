package service.database.generator;

import java.util.ArrayList;
import java.util.List;

public class SelectorGenerator {
    private final int outputBytes;

    public SelectorGenerator(int outputBytes) {
        assert outputBytes > 0;
        this.outputBytes = outputBytes;
    }

    public String generateSelector(String string){
        int[] hashMask = new int[outputBytes];
        char[] chars = string.toCharArray();

        for (int i = 0; i < chars.length; i++){
            int maskIndex = (i % hashMask.length) & Integer.MAX_VALUE;
            int currentHash = hashMask[maskIndex];
            int newHash = chars[i];
            hashMask[maskIndex] = (currentHash ^ newHash);
        }

        List<String> parts = new ArrayList<>(hashMask.length);
        for (int value : hashMask) {
            StringBuilder hexString = new StringBuilder(Integer.toHexString(value));
            if (hexString.length() > 2) {
                hexString = new StringBuilder(hexString.substring(hexString.length() - 2));
            }
            while (hexString.length() < 2) {
                hexString.insert(0, "0");
            }
            parts.add(hexString.toString());
        }
        return String.join("-", parts);
    }
}
