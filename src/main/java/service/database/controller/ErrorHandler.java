package service.database.controller;

import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;

@RestController
@RequestMapping(ErrorHandler.ERROR_PAGE_PATH)
public class ErrorHandler extends AbstractErrorController {
    static final String ERROR_PAGE_PATH = "/error";

    public ErrorHandler(ErrorAttributes errorAttributes) {
        super(errorAttributes, Collections.emptyList());
    }

    @RequestMapping
    public ResponseEntity<Map<String, Object>> error(HttpServletRequest request) {
        Collection<ErrorAttributeOptions.Include> includes = new HashSet<>();
        includes.add(ErrorAttributeOptions.Include.EXCEPTION);
        includes.add(ErrorAttributeOptions.Include.MESSAGE);
        Map<String, Object> body = this.getErrorAttributes(request, ErrorAttributeOptions.of(includes));
        HttpStatus status = this.getStatus(request);
        return new ResponseEntity<>(body, status);
    }

    @Override
    public String getErrorPath() {
        return ERROR_PAGE_PATH;
    }
}
