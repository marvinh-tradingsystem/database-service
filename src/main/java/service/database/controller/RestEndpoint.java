package service.database.controller;

import databaseconnector.api.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import service.database.controller.rest.request.*;
import service.database.controller.rest.response.InitializationStatus;
import service.database.controller.rest.response.ProductPriceInsertionStatus;
import service.database.service.DatabaseCommunicationService;
import service.database.service.RestSelectorGeneratorService;
import service.database.service.database.CurrencyEntry;
import service.database.service.database.ProductEntry;

import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

@RestController
public class RestEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestEndpoint.class);

    private final DatabaseCommunicationService databaseCommunicationService;
    private final RestSelectorGeneratorService selectorGeneratorService;

    @Autowired
    public RestEndpoint(DatabaseCommunicationService databaseCommunicationService, RestSelectorGeneratorService selectorGeneratorService) {
        this.databaseCommunicationService = databaseCommunicationService;
        this.selectorGeneratorService = selectorGeneratorService;
    }

    @GetMapping("/database/initiated")
    public InitializationStatus initiated(){
        return new InitializationStatus(databaseCommunicationService.isDatabaseInitiated());
    }

    @PostMapping("/database/init")
    public InitializationStatus initiate(){
        databaseCommunicationService.initDatabase();
        return new InitializationStatus(true);
    }

    @PostMapping("/product/price/store")
    public ProductPriceInsertionStatus storeProductPrice(@RequestBody ProductPrice productPrice) throws NoSuchAlgorithmException, ConstraintViolationException {
        ProductEntry productEntry = databaseCommunicationService.createEntryIfNotExists(productPrice.getProduct());
        CurrencyEntry currencyEntry = databaseCommunicationService.createEntryIfNotExists(productPrice.getPrice().getCurrency());
        databaseCommunicationService.createEntry(
                currencyEntry, productPrice.getPrice().getValue(), productEntry,
                ZonedDateTime.parse(productPrice.getDateTime(), DateTimeFormatter.ISO_ZONED_DATE_TIME)
        );
        return new ProductPriceInsertionStatus(true);
    }

    @GetMapping(value = {
            "/product/price",
            "/product/sel-{selector}/price"
    }, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ProductPrice> getProductPrice(
            @PathVariable(name = "selector", required = false) String selector,
            @RequestParam(name = "fromDate", required = false) String fromDate,
            @RequestParam(name = "toDate", required = false) String toDate
    ){
        return Flux.fromStream(databaseCommunicationService.readProductPrices(productPriceEntry -> {
            String productSelector = selectorGeneratorService.generateSelector(productPriceEntry.getProductEntry().getId());
            if (fromDate != null){
                DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                        .append(DateTimeFormatter.ISO_OFFSET_DATE)
                        .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                        .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                        .toFormatter();
                ZonedDateTime fromDateTime = ZonedDateTime.parse(fromDate, formatter);
                if (productPriceEntry.getDateTime().compareTo(fromDateTime) < 0){
                    return false;
                }
            }
            if (toDate != null){
                DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                        .append(DateTimeFormatter.ISO_OFFSET_DATE)
                        .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                        .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
                        .toFormatter();
                ZonedDateTime toDateTime = ZonedDateTime.parse(toDate, formatter);
                toDateTime = toDateTime.plusDays(1);
                if (productPriceEntry.getDateTime().compareTo(toDateTime) >= 0){
                    return false;
                }
            }
            return selector == null || productSelector.equals(selector);
        }).map(productPriceEntry -> {
            Product product = productPriceEntry.getProductEntry().getProduct();
            Price price = new Price(productPriceEntry.getPrice(), productPriceEntry.getCurrencyEntry().getCurrency());
            ZonedDateTime dateTime = productPriceEntry.getDateTime();
            return new ProductPrice(product, price, dateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME));
        }));
    }

    @GetMapping(value = {
            "/product",
            "/product/sel-{selector}"
    }, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<SelectableProduct> getProduct(
            @PathVariable(name = "selector", required = false) String selector
    ){
        return Flux.fromStream(databaseCommunicationService.readProducts(productEntry -> {
            String productSelector = selectorGeneratorService.generateSelector(productEntry.getId());
            return selector == null || productSelector.equals(selector);
        }).map(productEntry -> {
            String productSelector = selectorGeneratorService.generateSelector(productEntry.getId());
            return new SelectableProduct(productSelector, productEntry.getProduct());
        }));
    }

    @GetMapping(value = {
            "/currency",
            "/currency/sel-{selector}"
    }, produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<SelectableCurrency> getCurrency(
            @PathVariable(name = "selector", required = false) String selector
    ){
        return Flux.fromStream(databaseCommunicationService.readCurrencies(currencyEntry -> {
            String currencySelector = selectorGeneratorService.generateSelector(currencyEntry.getId());
            return selector == null || currencySelector.equals(selector);
        }).map(currencyEntry -> {
            String currencySelector = selectorGeneratorService.generateSelector(currencyEntry.getId());
            return new SelectableCurrency(currencySelector, currencyEntry.getCurrency());
        }));
    }
}
