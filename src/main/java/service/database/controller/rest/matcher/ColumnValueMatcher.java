package service.database.controller.rest.matcher;

import databaseconnector.api.Column;
import databaseconnector.api.DatabaseConnection;
import databaseconnector.api.sql.SQLDatabaseConnection;

import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class ColumnValueMatcher implements Predicate<SQLDatabaseConnection.Row> {
    private final Map<String, String> columnValues;

    public ColumnValueMatcher(Map<String, String> columnValues) {
        this.columnValues = columnValues;
    }

    @Override
    public boolean test(SQLDatabaseConnection.Row row) {
        return columnValues.entrySet().stream().allMatch(columnValueEntry -> {
            Optional<Column> column = row.getColumns().stream().filter(column1 -> column1.getName().equalsIgnoreCase(columnValueEntry.getKey())).findAny();
            if (!column.isPresent()){
                return false;
            }
            Optional<DatabaseConnection.Value> value = row.get(column.get());
            if (columnValueEntry.getValue() == null){
                return !value.isPresent();
            }

            //noinspection OptionalIsPresent
            if (!value.isPresent()){
                return false;
            }
            return columnValueEntry.getValue().equals(value.get().get());
        });
    }
}