package service.database.controller.rest.matcher;

import databaseconnector.api.sql.SQLDatabaseConnection;

import java.util.Map;

public class RowMatcher extends ColumnValueMatcher {
    private final String tableName;

    public RowMatcher(String tableName, Map<String, String> columnValues) {
        super(columnValues);
        this.tableName = tableName;
    }

    @Override
    public boolean test(SQLDatabaseConnection.Row row) {
        return tableName.equalsIgnoreCase(row.getTable().getName()) && super.test(row);
    }
}
