package service.database.controller.rest.request;

public class SelectableCurrency {
    private String selector;
    private Currency currency;

    public SelectableCurrency() {
    }

    public SelectableCurrency(String selector, Currency currency) {
        this.selector = selector;
        this.currency = currency;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
}
