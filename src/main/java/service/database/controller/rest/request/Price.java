package service.database.controller.rest.request;

import java.math.BigDecimal;

public class Price {
    private BigDecimal value;
    private Currency currency;

    public Price() {}

    public Price(BigDecimal value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    public BigDecimal getValue() {
        return value;
    }

    public Currency getCurrency() {
        return currency;
    }
}
