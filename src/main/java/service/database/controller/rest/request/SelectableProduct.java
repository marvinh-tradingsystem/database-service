package service.database.controller.rest.request;

public class SelectableProduct {
    private String selector;
    private Product product;

    public SelectableProduct() {
    }

    public SelectableProduct(String selector, Product product) {
        this.selector = selector;
        this.product = product;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
