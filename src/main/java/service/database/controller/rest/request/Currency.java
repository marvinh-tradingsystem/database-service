package service.database.controller.rest.request;

public class Currency {
    private String abbreviation;
    private String name;
    private String symbol;

    public Currency() {}

    public Currency(String abbreviation, String name, String symbol) {
        this.abbreviation = abbreviation;
        this.name = name;
        this.symbol = symbol;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }
}
