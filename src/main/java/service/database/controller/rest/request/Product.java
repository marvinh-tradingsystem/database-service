package service.database.controller.rest.request;

public class Product {
    private String name;

    public Product() {}

    public Product(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
