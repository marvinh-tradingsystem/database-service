package service.database.controller.rest.response;

public class InitializationStatus {
    private final boolean initiated;

    public InitializationStatus(boolean initiated) {
        this.initiated = initiated;
    }

    public boolean isInitiated() {
        return initiated;
    }
}
