package service.database.controller.rest.response;

public class ProductPriceInsertionStatus {
    private final boolean insertionSuccessful;

    public ProductPriceInsertionStatus(boolean insertionSuccessful) {
        this.insertionSuccessful = insertionSuccessful;
    }

    public boolean isInsertionSuccessful() {
        return insertionSuccessful;
    }
}
