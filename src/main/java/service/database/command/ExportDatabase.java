package service.database.command;

import databaseconnector.api.Column;
import databaseconnector.api.DatabaseConnection;
import databaseconnector.api.exception.TableNotExistsException;
import databaseconnector.api.sql.SQLDatabaseConnection;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import service.database.command.exception.TableNotFoundException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@WebServlet("/command/database/export")
public class ExportDatabase extends HttpServlet {
    @WebFilter("/command/database/export")
    private static class ContentTypeFilter implements Filter {
        @Override
        public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
            if (servletResponse instanceof HttpServletResponse){
                ((HttpServletResponse) servletResponse).setHeader("Content-Type", "application/zip");
            }
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private static class RuntimeIOException extends RuntimeException {
        public RuntimeIOException(Throwable cause) {
            super(cause);
        }
    }

    private static class RuntimeTableNotExistsException extends RuntimeException {
        public RuntimeTableNotExistsException(Throwable cause) {
            super(cause);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(this.getServletContext());
        SQLDatabaseConnection databaseConnection = (SQLDatabaseConnection) context.getBean("database.connection");

        Pattern tablePattern = getTablePattern(req);

        Set<String> tableNames = databaseConnection.read(row -> tablePattern.asPredicate().test(row.getTable().getName()))
                .map(row -> row.getTable().getName().toLowerCase())
                .collect(Collectors.toSet());

        try (ZipOutputStream zipOutputStream = new ZipOutputStream(resp.getOutputStream(), StandardCharsets.UTF_8)){
            tableNames.forEach(tableName -> {
                ZipEntry zipEntry = new ZipEntry(String.format("%s.csv", tableName));
                try {
                    zipOutputStream.putNextEntry(zipEntry);
                } catch (IOException e) {
                    throw new RuntimeIOException(e);
                }
                PrintWriter writer = new PrintWriter(zipOutputStream, true);

                try {
                    List<String> columnNames = databaseConnection.getColumns(tableName).stream()
                            .map(Column::getName)
                            .collect(Collectors.toList());
                    writer.println(String.join(";", columnNames));
                    databaseConnection.read(row -> row.getTable().getName().equalsIgnoreCase(tableName)).forEach(row -> {
                        List<String> values = columnNames.stream().map(columnName -> {
                            Optional<Column> rowColumn = row.getColumns().stream()
                                    .filter(column -> column.getName().equalsIgnoreCase(columnName))
                                    .findAny();
                            if (!rowColumn.isPresent()){
                                return "";
                            }
                            Optional<DatabaseConnection.Value> value = row.get(rowColumn.get());
                            if (!value.isPresent()){
                                return "";
                            }
                            return value.get().get();
                        }).collect(Collectors.toList());
                        String valueString = String.join(";", values);
                        writer.println(valueString);
                    });
                } catch (TableNotExistsException e) {
                    throw new RuntimeTableNotExistsException(e);
                }
            });
        } catch (RuntimeIOException exception){
            throw new IOException(exception.getCause());
        } catch (RuntimeTableNotExistsException exception){
            throw new TableNotFoundException(exception.getCause());
        }
    }

    @SuppressWarnings("DuplicatedCode")
    private static Pattern getTablePattern(HttpServletRequest request){
        Pattern tablePattern = Pattern.compile(".*", Pattern.CASE_INSENSITIVE);
        if (request.getParameterMap().containsKey("table")){
            String pattern = URLDecoder.decode(request.getParameter("table"), StandardCharsets.UTF_8);
            if (pattern != null){
                tablePattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            }
        }
        return tablePattern;
    }
}
