package service.database.command.exception;

import javax.servlet.ServletException;

public class InvalidRequestDataException extends ServletException {
    public InvalidRequestDataException(String message) {
        super(message);
    }
}
