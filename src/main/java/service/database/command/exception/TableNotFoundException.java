package service.database.command.exception;

import javax.servlet.ServletException;

public class TableNotFoundException extends ServletException {
    public TableNotFoundException(String message) {
        super(message);
    }

    public TableNotFoundException(Throwable rootCause) {
        super(rootCause);
    }
}
