package service.database.service.database;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class ProductPriceEntry {
    private final CurrencyEntry currencyEntry;
    private final BigDecimal price;
    private final ProductEntry productEntry;
    private final ZonedDateTime dateTime;

    public ProductPriceEntry(
            CurrencyEntry currencyEntry, BigDecimal price,
            ProductEntry productEntry, ZonedDateTime dateTime
    ) {
        this.currencyEntry = currencyEntry;
        this.price = price;
        this.productEntry = productEntry;
        this.dateTime = dateTime;
    }

    public CurrencyEntry getCurrencyEntry() {
        return currencyEntry;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public ProductEntry getProductEntry() {
        return productEntry;
    }

    public ZonedDateTime getDateTime() {
        return dateTime;
    }
}
