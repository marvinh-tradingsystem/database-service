package service.database.service.database.functions;

import databaseconnector.api.sql.SQLDatabaseConnection;
import service.database.controller.rest.request.Currency;
import service.database.schema.CurrencyTable;
import service.database.service.database.CurrencyEntry;

import java.util.function.Function;

public class RowToCurrencyEntry implements Function<SQLDatabaseConnection.Row, CurrencyEntry> {
    @Override
    public CurrencyEntry apply(SQLDatabaseConnection.Row row) {
        assert row.getTable().getName().equalsIgnoreCase(new CurrencyTable().getName());
        assert row.get(CurrencyTable.ABBREVIATION).isPresent();
        String abbreviation = row.get(CurrencyTable.ABBREVIATION).get().get();
        assert row.get(CurrencyTable.NAME).isPresent();
        String name = row.get(CurrencyTable.NAME).get().get();
        assert row.get(CurrencyTable.SYMBOL).isPresent();
        String symbol = row.get(CurrencyTable.SYMBOL).get().get();
        return new CurrencyEntry(abbreviation, new Currency(abbreviation, name, symbol));
    }
}
