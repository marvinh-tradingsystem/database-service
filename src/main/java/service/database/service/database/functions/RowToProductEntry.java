package service.database.service.database.functions;

import databaseconnector.api.sql.SQLDatabaseConnection;
import service.database.controller.rest.request.Product;
import service.database.schema.ProductTable;
import service.database.service.database.ProductEntry;

import java.util.function.Function;

public class RowToProductEntry implements Function<SQLDatabaseConnection.Row, ProductEntry> {
    @Override
    public ProductEntry apply(SQLDatabaseConnection.Row row) {
        assert row.getTable().getName().equalsIgnoreCase(new ProductTable().getName());
        assert row.get(ProductTable.NAME).isPresent();
        String name = row.get(ProductTable.NAME).get().get();
        return new ProductEntry(name, new Product(name));
    }
}
