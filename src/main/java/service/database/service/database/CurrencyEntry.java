package service.database.service.database;

import service.database.controller.rest.request.Currency;

public class CurrencyEntry {
    private final String id;
    private final Currency currency;

    public CurrencyEntry(String id, Currency currency) {
        this.id = id;
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public Currency getCurrency() {
        return currency;
    }
}
