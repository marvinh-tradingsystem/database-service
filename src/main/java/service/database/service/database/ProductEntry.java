package service.database.service.database;

import service.database.controller.rest.request.Product;

public class ProductEntry {
    private final String id;
    private final Product product;

    public ProductEntry(String id, Product product) {
        this.id = id;
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public Product getProduct() {
        return product;
    }
}
