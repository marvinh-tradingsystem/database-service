package service.database.service;

import org.springframework.stereotype.Service;
import service.database.generator.SelectorGenerator;

@Service
public class RestSelectorGeneratorService extends SelectorGenerator {
    public RestSelectorGeneratorService() {
        super(5);
    }
}
