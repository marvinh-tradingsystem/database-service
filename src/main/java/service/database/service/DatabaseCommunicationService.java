package service.database.service;

import databaseconnector.api.Column;
import databaseconnector.api.DatabaseConnection;
import databaseconnector.api.exception.ConstraintViolationException;
import databaseconnector.api.sql.SQLDatabaseConnection;
import databaseconnector.api.sql.SQLSchema;
import databaseconnector.api.sql.SQLTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import service.database.controller.rest.request.Currency;
import service.database.controller.rest.request.Product;
import service.database.schema.CurrencyTable;
import service.database.schema.ProductPriceTable;
import service.database.schema.ProductTable;
import service.database.service.database.CurrencyEntry;
import service.database.service.database.ProductEntry;
import service.database.service.database.ProductPriceEntry;
import service.database.service.database.functions.RowToCurrencyEntry;
import service.database.service.database.functions.RowToProductEntry;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

@Service
public class DatabaseCommunicationService {
    @Resource(name="database.connection")
    @Lazy
    private SQLDatabaseConnection databaseConnection;

    @Resource(name = "database.schema")
    @Lazy
    private SQLSchema schema;

    private interface Generator<G, O>{
        G generate(O object);
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseCommunicationService.class);

    public boolean isDatabaseInitiated(){
        return databaseConnection.isInitiated(schema);
    }

    public void initDatabase(){
        databaseConnection.init(schema);
    }

    public ProductEntry createEntryIfNotExists(Product product) throws NoSuchAlgorithmException {
        return createEntryIfNotExists(product, row -> {
            if (!row.getTable().getName().equalsIgnoreCase(new ProductTable().getName())) return false;
            if (!row.get(ProductTable.NAME).isPresent() || !row.get(ProductTable.NAME).get().get().equals(product.getName())) {
                return false;
            }
            return true;
        }, new RowToProductEntry(), object -> {
            SQLTable table = new ProductTable();
            Map<Column, DatabaseConnection.Value> columnValues = Map.of(
                    ProductTable.NAME, new DatabaseConnection.Value(object.getName())
            );
            return new SQLDatabaseConnection.Row(columnValues, table);
        });
    }

    public CurrencyEntry createEntryIfNotExists(Currency currency){
        return createEntryIfNotExists(currency, new Predicate<SQLDatabaseConnection.Row>() {
            @Override
            public boolean test(SQLDatabaseConnection.Row row) {
                if (!row.getTable().getName().equalsIgnoreCase(new CurrencyTable().getName())) return false;
                if (!row.get(CurrencyTable.ABBREVIATION).isPresent() || !row.get(CurrencyTable.ABBREVIATION).get().get().equals(currency.getAbbreviation())) {
                    return false;
                }
                return true;
            }
        }, new RowToCurrencyEntry(), object -> {
            SQLTable table = new CurrencyTable();
            Map<Column, DatabaseConnection.Value> columnValues = Map.of(
                    CurrencyTable.ABBREVIATION, new DatabaseConnection.Value(object.getAbbreviation()),
                    CurrencyTable.NAME, new DatabaseConnection.Value(object.getName()),
                    CurrencyTable.SYMBOL, new DatabaseConnection.Value(object.getSymbol())
            );
            return new SQLDatabaseConnection.Row(columnValues, table);
        });
    }

    public ProductPriceEntry createEntry(
            CurrencyEntry currencyEntry, BigDecimal price,
            ProductEntry productEntry, ZonedDateTime dateTime
    ) throws ConstraintViolationException {
        SQLTable table = new ProductPriceTable();
        Map<Column, DatabaseConnection.Value> columnValues = Map.of(
                ProductPriceTable.PRICE, new DatabaseConnection.Value(price.toString()),
                ProductPriceTable.PRODUCT_ID, new DatabaseConnection.Value(productEntry.getId()),
                ProductPriceTable.CURRENCY_ID, new DatabaseConnection.Value(currencyEntry.getId()),
                ProductPriceTable.DATETIME, new DatabaseConnection.Value(dateTime.format(DateTimeFormatter.ISO_ZONED_DATE_TIME))
        );
        databaseConnection.insert(new SQLDatabaseConnection.Row(columnValues, table));
        return new ProductPriceEntry(currencyEntry, price, productEntry, dateTime);
    }

    private <E, O> E createEntryIfNotExists(
            O object, Predicate<SQLDatabaseConnection.Row> filter,
            Function<SQLDatabaseConnection.Row, E> rowToEntry,
            Generator<SQLDatabaseConnection.Row, O> rowGenerator
    ){
        Optional<E> entry = databaseConnection.read(filter).map(rowToEntry).findAny();
        if (entry.isPresent()){
            return entry.get();
        }
        while (true){
            SQLDatabaseConnection.Row row = rowGenerator.generate(object);
            try {
                databaseConnection.insert(row);
                entry = Optional.of(rowToEntry.apply(row));
                break;
            } catch (ConstraintViolationException e) {
                LOGGER.warn(String.format("Saving row failed.\nException: %s\nRow: %s\nRegenerating row.", e.getMessage(), row.toString()));
            }
        }
        return entry.get();
    }

    public Stream<ProductEntry> readProducts(Predicate<ProductEntry> filter){
        return databaseConnection
                .read(row -> row.getTable().getName().equalsIgnoreCase(new ProductTable().getName()))
                .map(new RowToProductEntry())
                .filter(filter);
    }

    public Stream<CurrencyEntry> readCurrencies(Predicate<CurrencyEntry> filter){
        return databaseConnection
                .read(row -> row.getTable().getName().equalsIgnoreCase(new CurrencyTable().getName()))
                .map(new RowToCurrencyEntry())
                .filter(filter);
    }

    public Stream<ProductPriceEntry> readProductPrices(Predicate<ProductPriceEntry> filter){
        Map<String, Optional<CurrencyEntry>> currencyCache = new HashMap<>();
        Map<String, Optional<ProductEntry>> productCache = new HashMap<>();

        return databaseConnection.read(row -> row.getTable().getName().equalsIgnoreCase(new ProductPriceTable().getName())).map(new Function<SQLDatabaseConnection.Row, ProductPriceEntry>() {
            @Override
            public ProductPriceEntry apply(SQLDatabaseConnection.Row row) {
                assert row.get(ProductPriceTable.DATETIME).isPresent();
                ZonedDateTime zonedDateTime = ZonedDateTime.parse(
                        row.get(ProductPriceTable.DATETIME).get().get(),
                        DateTimeFormatter.ISO_ZONED_DATE_TIME
                );
                assert row.get(ProductPriceTable.PRICE).isPresent();
                BigDecimal price = new BigDecimal(row.get(ProductPriceTable.PRICE).get().get());
                assert row.get(ProductPriceTable.PRODUCT_ID).isPresent();
                String productID = row.get(ProductPriceTable.PRODUCT_ID).get().get();
                if (!productCache.containsKey(productID)){
                    productCache.put(productID, readProducts(productEntry -> productEntry.getId().equals(productID)).findAny());
                }
                Optional<ProductEntry> productEntryOptional = productCache.get(productID);
                assert productEntryOptional.isPresent();
                assert row.get(ProductPriceTable.CURRENCY_ID).isPresent();
                String currencyID = row.get(ProductPriceTable.CURRENCY_ID).get().get();
                if (!currencyCache.containsKey(currencyID)){
                    currencyCache.put(currencyID, readCurrencies(currencyEntry -> currencyEntry.getId().equals(currencyID)).findAny());
                }
                Optional<CurrencyEntry> currencyEntryOptional = currencyCache.get(currencyID);
                assert currencyEntryOptional.isPresent();
                return new ProductPriceEntry(currencyEntryOptional.get(), price, productEntryOptional.get(), zonedDateTime);
            }
        }).filter(filter);
    }
}
