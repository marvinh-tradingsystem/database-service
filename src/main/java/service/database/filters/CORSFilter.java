package service.database.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(value = "*", asyncSupported = true)
public class CORSFilter implements Filter {
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if ((servletRequest instanceof HttpServletRequest) && (servletResponse instanceof HttpServletResponse)){
            String origin = ((HttpServletRequest) servletRequest).getHeader("Origin");
            if (origin != null){
                ((HttpServletResponse) servletResponse).setHeader("Access-Control-Allow-Origin", origin);
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
