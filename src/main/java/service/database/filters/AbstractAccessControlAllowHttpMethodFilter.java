package service.database.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class AbstractAccessControlAllowHttpMethodFilter implements Filter {
    private final String allowMethod;

    protected AbstractAccessControlAllowHttpMethodFilter(String allowMethod) {
        this.allowMethod = allowMethod;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if ((servletRequest instanceof HttpServletRequest) && (servletResponse instanceof HttpServletResponse)){
            String methods = ((HttpServletRequest) servletRequest).getHeader("Access-Control-Allow-Methods");
            if (methods == null){
                methods = "";
            }
            Set<String> allowedMethods = Arrays.stream(methods.split(",")).map(String::trim).collect(Collectors.toSet());
            allowedMethods.add(allowMethod);
            ((HttpServletResponse) servletResponse).setHeader("Access-Control-Allow-Methods", String.join(", ", allowedMethods));
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
