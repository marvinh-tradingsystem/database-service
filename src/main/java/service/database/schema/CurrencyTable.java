package service.database.schema;

import databaseconnector.api.Column;
import databaseconnector.api.sql.SQLTable;
import databaseconnector.api.sql.constraint.Constraint;
import databaseconnector.api.sql.constraint.NotNull;
import databaseconnector.api.sql.constraint.PrimaryKey;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CurrencyTable implements SQLTable {
    public static final Column ABBREVIATION = new Column() {
        @Override
        public String getName() {
            return "abbreviation";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    public static final Column NAME = new Column() {
        @Override
        public String getName() {
            return "name";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    public static final Column SYMBOL = new Column() {
        @Override
        public String getName() {
            return "symbol";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    @Override
    public Set<Constraint> getConstraints() {
        return new HashSet<>(Arrays.asList(
                new PrimaryKey(ABBREVIATION),
                new NotNull(NAME)
        ));
    }

    @Override
    public String getName() {
        return "currency";
    }

    @Override
    public Set<Column> getColumns() {
        return new HashSet<>(Arrays.asList(
                ABBREVIATION,
                NAME,
                SYMBOL
        ));
    }
}
