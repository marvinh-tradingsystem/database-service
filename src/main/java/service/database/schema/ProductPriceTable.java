package service.database.schema;

import databaseconnector.api.Column;
import databaseconnector.api.sql.SQLTable;
import databaseconnector.api.sql.constraint.Constraint;
import databaseconnector.api.sql.constraint.NotNull;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ProductPriceTable implements SQLTable {
    public static final Column PRODUCT_ID = new Column() {
        @Override
        public String getName() {
            return "product_id";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    public static final Column PRICE = new Column() {
        @Override
        public String getName() {
            return "price";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    public static final Column CURRENCY_ID = new Column() {
        @Override
        public String getName() {
            return "currency_id";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    public static final Column DATETIME = new Column() {
        @Override
        public String getName() {
            return "datetime";
        }

        @Override
        public String getDatatype() {
            return "VARCHAR(255)";
        }
    };

    @Override
    public Set<Constraint> getConstraints() {
        return new HashSet<>(Arrays.asList(
                new NotNull(PRODUCT_ID),
                new NotNull(PRICE),
                new NotNull(CURRENCY_ID),
                new NotNull(DATETIME)
        ));
    }

    @Override
    public String getName() {
        return "product_price";
    }

    @Override
    public Set<Column> getColumns() {
        return new HashSet<>(Arrays.asList(
                PRODUCT_ID,
                PRICE,
                CURRENCY_ID,
                DATETIME
        ));
    }
}
