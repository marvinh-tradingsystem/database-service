package service.database.schema;

import databaseconnector.api.sql.SQLSchema;
import databaseconnector.api.sql.SQLTable;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class DatabaseSchema implements SQLSchema {
    @Override
    public Set<SQLTable> getTables() {
        return new HashSet<>(Arrays.asList(
                new ProductTable(),
                new ProductPriceTable(),
                new CurrencyTable()
        ));
    }
}
