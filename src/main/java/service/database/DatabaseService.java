package service.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.io.File;

@Lazy
@Configuration
@ServletComponentScan
@SpringBootApplication
public class DatabaseService implements ApplicationRunner {
    @Value("${database.file:database.h2}")
    private String databaseFile;

    private final DatabaseConfigurationService databaseConfigurationService;

    @Autowired
    public DatabaseService(DatabaseConfigurationService databaseConfigurationService) {
        this.databaseConfigurationService = databaseConfigurationService;
    }

    public static void main(String[] args){
        SpringApplication.run(DatabaseService.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        databaseConfigurationService.setDatabaseFile(new File(databaseFile));
    }
}
